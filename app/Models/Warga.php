<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
    use HasFactory;
    protected $table = 'warga'; //memberitahukan table warga not wargas. But default add s / plural
    protected $guarded = []; //semua filed able unutk dimasukan
}
