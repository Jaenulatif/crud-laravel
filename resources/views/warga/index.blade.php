@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Warga') }}</div>

                <div class="card-body">
                    <a class="btn btn-primary my-3" href="/warga/create">Add Warga</a>
                    <table border="1" class="table table-hover">
                        <tr>
                            <th>Id</th>
                            <th>NAMA</th>
                            <th>NIK</th>
                            <th>NO.KK</th>
                            <th>JENIS KELAMIN</th>
                            <th>ALAMAT</th>
                            <th>Aksi</th>
                        </tr>
                        @foreach($warga as $w)
                        <tr>
                            <td>{{$w->id}}</td>
                            <td>{{$w->nama}}</td>
                            <td>{{$w->nik}}</td>
                            <td>{{$w->no_kk}}</td>
                            <td>{{$w->jenis_kelamin}}</td>
                            <td>{{$w->alamat}}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-warning me-2" href="/warga/{{$w->id}}/edit">Edit</a>
                                    <form action="/warga/{{$w->id}}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input class="btn btn-danger" type="submit" value="Delete" onclick="confirm('Sure ?')" )>
                                    </form>
                                </div>
                            </td>

                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection