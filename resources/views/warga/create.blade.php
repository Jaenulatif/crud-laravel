@extends('layout.main')
@section('content')
<div class="container">
    <h1>Create Warga</h1>
    <form action="/warga/store" method="POST">
        @csrf
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="masukan nama">
        </div>
        <div class="mb-3">
            <label for="nik" class="form-label">NIK</label>
            <input type="text" class="form-control" id="nik" name="nik" placeholder="masukan nik">
        </div>
        <div class="mb-3">
            <label for="no_kk" class="form-label">Nomor KK</label>
            <input type="text" class="form-control" id="no_kk" name="no_kk" placeholder="masukan nomor KK">
        </div>
        <select class="form-select" name="jenis_kelamin">
            <option value="">Pilih Jenis Kelamin</option>
            <option value="L">Laki-Laki</option>
            <option value="P">Perempuan</option>
        </select><br>
        <div class="mb-3">
            <label for="alamat" class="form-label">Alamat</label>
            <textarea class="form-control" name="alamat" rows="10" id="alamat"></textarea><br>
        </div>
        <input class="btn btn-info" type="submit" name="submit" value="Save">
    </form>
</div>
@endsection